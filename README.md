# stylebot #

### What is this repository for? ###

* bot that runs astyle
* version 0.1.0

### How do I get set up? ###

* get [docker](https://www.docker.com/)
* `docker run nroza/stylebot`
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
